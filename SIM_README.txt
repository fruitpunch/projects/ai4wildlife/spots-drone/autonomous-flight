"""
Below are the instructions to create the docker container to run the simulation
environment.

It is validated for Linux LTS versions. It takes about 30 mins to set up
everything after the installation of docker.
"""

1. Install docker using this link (https://docs.docker.com/engine/install/)

2. Highly recommend either Linux 18 or 20. You can pull the relevant version
using this page: https://hub.docker.com/_/ubuntu/?tab=description

3. Run the series of commands given below, after running the image that you
just downloaded.
a. apt install git
b. git clone https://github.com/PX4/PX4-Autopilot.git --recursive
c. ./PX4-Autopilot/Tools/setup/ubuntu.sh

4. After the image has been set up as above, what you can do is commit it as a
new image, by using the 'docker commit' command.

5. You should be able to then run the new image that you have saved to run the
simulations